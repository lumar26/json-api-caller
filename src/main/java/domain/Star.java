package domain;

import java.net.URL;
import java.util.Objects;

public class Star {
    private String rank;
    private String name;
    private String visualMagnitude;
    private String distance;
    private String spectralClass;
    private URL link;

    public Star() {
    }

    public Star(String rank, String name, String visualMagnitude, String distance, String spectralClass, URL link) {
        this.rank = rank;
        this.name = name;
        this.visualMagnitude = visualMagnitude;
        this.distance = distance;
        this.spectralClass = spectralClass;
        this.link = link;
    }

    @Override
    public String toString() {
        return "Star{" +
                "rank='" + rank + '\'' +
                ", name='" + name + '\'' +
                ", visualMagnitude='" + visualMagnitude + '\'' +
                ", distance='" + distance + '\'' +
                ", spectralClass='" + spectralClass + '\'' +
                ", link=" + link +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Star)) return false;
        Star star = (Star) o;
        return rank.equals(star.rank) && name.equals(star.name) && visualMagnitude.equals(star.visualMagnitude) && distance.equals(star.distance) && spectralClass.equals(star.spectralClass) && link.equals(star.link);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rank, name, visualMagnitude, distance, spectralClass, link);
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVisualMagnitude() {
        return visualMagnitude;
    }

    public void setVisualMagnitude(String visualMagnitude) {
        this.visualMagnitude = visualMagnitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSpectralClass() {
        return spectralClass;
    }

    public void setSpectralClass(String spectralClass) {
        this.spectralClass = spectralClass;
    }

    public URL getLink() {
        return link;
    }

    public void setLink(URL link) {
        this.link = link;
    }
}
