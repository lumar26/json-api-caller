package rs.ac.bg.fon.np.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import domain.Star;

import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

public class Main3 {
    public static void main(String[] args) {
        Gson gson = new Gson();

        try (FileReader fr = new FileReader("stars_array.json")){

            List<Star> stars = Arrays.asList(gson.fromJson(fr, Star[].class));
            stars.forEach(System.out::println);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
