package rs.ac.bg.fon.np.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import domain.Star;

import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;

public class Main4 {
    public static void main(String[] args) throws MalformedURLException {
        Star star = new Star("3", "Rigil Kentaurus & Toliman", "−0.27 (0.01 + 1.33)",
                "0004.4", "G2 V, K1 V", new URL("https://en.wikipedia.org/wiki/Alpha_Centauri"));

        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("rank", star.getRank());
        jsonObj.addProperty("name", star.getName());
        jsonObj.addProperty("visualMagnitude", star.getVisualMagnitude());
        jsonObj.addProperty("distance", star.getDistance());
        jsonObj.addProperty("spectralClass", star.getSpectralClass());
        jsonObj.addProperty("link", star.getLink().toString());

        try(FileWriter fw = new FileWriter("single_star.json")){
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            fw.write(gson.toJson(jsonObj));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
