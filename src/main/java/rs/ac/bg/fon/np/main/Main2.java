package rs.ac.bg.fon.np.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import domain.Star;

import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;

public class Main2 {
    public static void main(String[] args) throws MalformedURLException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Star[] stars = new Star[3];
        stars[0] = new Star("3", "Rigil Kentaurus & Toliman", "−0.27 (0.01 + 1.33)", "0004.4", "G2 V, K1 V", new URL("https://en.wikipedia.org/wiki/Alpha_Centauri"));
        stars[1] = new Star("6", "Procyon", "0.34",
                "0011", "F5 IV-V", new URL("https://en.wikipedia.org/wiki/Procyon"));

        stars[2] = new Star("7", "Capella", "0.08 (0.03–0.16)",
                "0043", "K0 III, G1 III", new URL("https://en.wikipedia.org/wiki/Capella"));

        try (FileWriter fw = new FileWriter("stars_array.json")){
            fw.write(gson.toJson(stars));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
