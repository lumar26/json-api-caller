package rs.ac.bg.fon.np.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import domain.Star;

import java.io.IOException;

public class Main {

    private static final String BASE_URL = "https://brightest-stars.p.rapidapi.com/brightstars";
    private static final String HOST_HEADER = "X-RapidAPI-Host";
    private static final String HOST_VALUE = "brightest-stars.p.rapidapi.com";
    private static final String KEY_HEADER = "X-RapidAPI-Key";
    private static final String KEY_VAUE = "";


    public static void main(String[] args) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(BASE_URL)
                .get()
                .addHeader(HOST_HEADER, HOST_VALUE)
                .addHeader(KEY_HEADER, KEY_VAUE)
                .build();

        Response response = client.newCall(request).execute();

        Gson gson = new GsonBuilder().create();
        Star[] stars = gson.fromJson(response.body().string(), Star[].class);
//        ne postoji endpoint za uzimanje samo jednog objekta, vec uvek vraca niz
        for (Star s : stars)
            System.out.println(s);
    }
}
